<?php

namespace App\Http\Resources;

use App\Models\Delegation;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Delegation */
class UserDelegationResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'start' => $this->start->format('Y-d-m H:m:i'),
            'end' => $this->end->format('Y-d-m H:m:i'),
            'country' => $this->country->code,
            'amount_due' => $this->amount_due,
            'currency' => $this->country->currency
        ];
    }
}
