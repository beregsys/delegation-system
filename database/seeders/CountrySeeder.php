<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{

    public function run()
    {
        $countries = [
            [
                'name' => 'Poland',
                'code' => 'PL',
                'daily_diet_cost' => 10,
                'currency' => 'PLN'
            ],
            [
                'name' => 'Denmark',
                'code' => 'DE',
                'daily_diet_cost' => 50,
                'currency' => 'PLN'
            ],
            [
                'name' => 'Great Britain',
                'code' => 'GB',
                'daily_diet_cost' => 75,
                'currency' => 'PLN'
            ]
        ];

        foreach ($countries as $country) {
            Country::create($country);
        }
    }
}
