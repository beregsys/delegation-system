<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DelegationStoreRequest;
use App\Http\Resources\DelegationResource;
use App\Models\Country;
use App\Models\Delegation;
use App\Models\User;
use Carbon\CarbonPeriod;
use Illuminate\Support\Carbon;

/**
 * @group Delegations
 *
 * Managing Delegations
 */

class DelegationController extends Controller
{
    public function store(DelegationStoreRequest $request): \Illuminate\Http\JsonResponse|DelegationResource
    {
        $start = Carbon::parse($request->input('start'));
        $end = Carbon::parse($request->input('end'));
        $user = User::findOrFail($request->input('user_id'));

        $hasDelegations = false;

        foreach ($user->delegations as $delegation) {
            if($delegation->start->between($start, $end) || $delegation->end->between($start, $end)) {
                $hasDelegations = true;
            }
        }

        if($hasDelegations) {
            return response()->json(['message' => 'User can have only one delegation in one time'], 400);
        }

        if(!in_array($request->input('code'), Country::CODES)) {
            return response()->json(['message' => 'Wrong country'], 400);
        }

        if($start->greaterThan($end)) {
            return response()->json(['message' => 'Start of delegation can\'t be greater then end'], 400);
        }


        $daysInTrip = CarbonPeriod::create($start, $end);
        $paidDaysCounter = 0;
        $unpaidDaysCounter = 0;
        $doublePaidDaysCounter = 0;
        foreach ($daysInTrip as $day) {

            $currentDay = $day->copy();
            if( !($daysInTrip->first()->equalTo($day) || $daysInTrip->last()->equalTo($day)) ) {
                $currentDay->startOfDay();
            }

            if( $this->moreThan(8, $currentDay, $daysInTrip->first()->equalTo($currentDay))) {
                $paidDaysCounter++;
            }

            if($this->unpaidDays($day->dayOfWeek, [6, 0])) {
                $unpaidDaysCounter++;
            }

            if($paidDaysCounter > 7 && !$this->unpaidDays($day->dayOfWeek, [6, 0])) {
                $doublePaidDaysCounter++;
            }
        }

        $normalPaidDays = $paidDaysCounter - $unpaidDaysCounter - $doublePaidDaysCounter;

        $country = Country::ofCode($request->input('code'))->first();

        $amount_due = ($normalPaidDays * $country->daily_diet_cost) + ($doublePaidDaysCounter * $country->daily_diet_cost * 2);

        $delegation = $request->validated();

        $delegation['amount_due'] = $amount_due;
        $delegation['country_id'] = $country->id;

        $delegation = Delegation::create($delegation);

        return new DelegationResource($delegation);
    }

    protected function moreThan( int $hours, Carbon $day, bool $first = false): bool
    {
        if($first) {
            return $day->diffInHours($day->copy()->addDay()->startOfDay()) >= $hours;
        }
        return $day->diffInHours($day->copy()->startOfDay()) >= $hours || $day->diffInHours($day->copy()->addDay()->startOfDay()) >= $hours;
    }

    protected function unpaidDays( int $dayOfWeek, array $unpaid): bool
    {
        return in_array($dayOfWeek, $unpaid);
    }
}
