<?php

namespace Database\Factories;

use App\Models\Country;
use App\Models\Delegation;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class DelegationFactory extends Factory
{
    protected $model = Delegation::class;

    public function definition(): array
    {
        $dateStart = rand(-1, -7);
        $dateEnd = rand(1, 7);

        $users = User::all();

        $userId = null;

        foreach ($users as $user) {
            if(!$user->delegation) {
                $userId = $user->id;
                break;
            }
        }

        return [
            'start' => Carbon::now()->addDays($dateStart),
            'end' => Carbon::now()->addDays($dateEnd),
            'amount_due' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'country_id' => function () {
                return Country::inRandomOrder()->first()->id;
            },
            'user_id' => function () {
                return User::inRandomOrder()->first()->id;
            }
        ];
    }
}
