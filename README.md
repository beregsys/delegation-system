### Project installation
1. Clone project
2. Install dependencies
```
composer i
```
3. Rename .env.example -> .env
4. Generate key for application
```
php artisan key:generate
```
5. Create database and update env file with db name, login, password
6. Run migrations and seeders
```
php artisan migrate --seed 
```

7. Run application
```
php artisan serve
```


### Documentation for API [http://localhost:8000/docs](http://localhost:8000/docs)
