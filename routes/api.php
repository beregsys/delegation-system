<?php

use App\Http\Controllers\Api\DelegationController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\UserDelegationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/users', [UserController::class, 'store']);

Route::post('/delegations', [DelegationController::class, 'store']);

Route::resource('users.delegations', UserDelegationController::class);
