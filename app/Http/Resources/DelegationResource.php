<?php

namespace App\Http\Resources;

use App\Models\Delegation;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Delegation */
class DelegationResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'delegation_id' => $this->id,
        ];
    }
}
