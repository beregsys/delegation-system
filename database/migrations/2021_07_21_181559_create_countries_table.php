<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    public function up()
    {
        Schema::create(
            'countries',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name', 100);
                $table->string('code', 2);
                $table->decimal('daily_diet_cost', unsigned:true);
                $table->string('currency', 5);
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
