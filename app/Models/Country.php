<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    public const CODES = [
        'PL',
        'DE',
        'GB'
    ];

    protected $fillable = [
        'name',
        'code',
        'daily_diet_cost',
        'currency'
    ];

    protected $casts = [
        'daily_diet_cost' => 'integer'
    ];

    public function delegations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Delegation::class);
    }

    public function scopeOfCode(Builder $query, $code): Builder
    {
        return $query->where(['code' => $code]);
    }
}
