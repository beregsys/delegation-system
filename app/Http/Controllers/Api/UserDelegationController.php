<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserDelegationResource;
use App\Models\User;

/**
 * @group User Delegations
 *
 * Managing User Delegations
 */

class UserDelegationController extends Controller
{
    public function index(User $user): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return UserDelegationResource::collection($user->delegations);
    }
}
