<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DelegationStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'start' => ['required', 'date'],
            'end' => ['required', 'date'],
            'user_id' => ['required', 'integer'],
            'code' => ['required', 'string', 'min:2', 'max:2']
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
