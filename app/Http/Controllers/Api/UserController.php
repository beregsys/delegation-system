<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;

/**
 * @group Users
 *
 * Managing Users
 */
class UserController extends Controller
{

    /**
     * Store user
     *
     * Store user with random data to database
     */
    public function store(): UserResource
    {
        $user = User::factory()->create();

        return new UserResource($user);
    }
}
